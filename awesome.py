#!/usr/bin/env python2
import sys
import random
import socket
from socket import timeout
import json
import math
import Queue
from sets import Set

exhausted = Set()

# Todo:
#     Create checkDroid
#     Prevent shooting at spawn points
#     Target everyone within range
#     If target in range(2-5): send droid or mortar?
#         Mortar dmg = 20 + 18 = 38
#         Mortar range = 2
#         Droid dmg = 22 + 10 = 32
#         Droid range = 3 (path)

def checkLaserHit(psource,target,laserrange,nmap):
    source = psource[1]
    x1,y1 = source
    x2,y2 = target

    xmin = source[0] - laserrange - 2
    ymin = source[1] - laserrange - 2
    xmax = source[0] + laserrange + 2
    ymax = source[1] + laserrange + 2
    
    nr = len(nmap)
    nc = len(nmap[0])

    if xmin < 0:
        xmin = 0
    if ymin < 0:
        ymin = 0
    if xmax > nc:
        xmax = nc
    if ymax > nr:
        ymax = nr
    
    if source[0] == target[0]:
        x = source[0]

        if source[1] > target[1]:
            for y in xrange(source[1], source[1] - (laserrange + 1)):
                if y < 0 or y > nr:
                    return False

                elif (x,y) == target:
                    return ['right-up', psource, int(math.hypot(x2-x1, y2-y1))]

                elif nmap[y][x] == 'O' or nmap[y][x] == 'S':
                    return False       
        else:
            for y in xrange(source[1], source[1] + (laserrange + 1)):
                if y < 0 or y > nr:
                    return False

                elif (x,y) == target:
                    return ['left-down', psource, int(math.hypot(x2-x1, y2-y1))]

                elif nmap[y][x] == 'O' or nmap[y][x] == 'S':
                    return False

    elif source[1] == target[1]:
        y = source[1]
        if source[0] < target[0]:
            for x in xrange(source[0], source[0] + (laserrange + 1)):
                if x < 0 or x > nc:
                    return False

                elif (x,y) == target:
                    return ['right-down', psource, int(math.hypot(x2-x1, y2-y1))]

                elif nmap[y][x] == 'O' or nmap[y][x] == 'S':
                    return False
        else:
            for x in xrange(source[0], source[0] - (laserrange + 1)):
                if x < 0 or x > nc:
                    return False
                elif (x,y) == target:
                    return ['left-up', psource, int(math.hypot(x2-x1, y2-y1))]

                elif nmap[y][x] == 'O' or nmap[y][x] == 'S':
                    return False

    elif x1 - y1 == x2 - y2:
        if source[0] < target[0]:
            #print target
            for i in xrange(0,laserrange + 1):
                #print nmap[y1 + i][x1 + i],
                #print (x1+i,y1+i),

                if x1 + i < 0 or x1 + i > nc or y1 + i < 0 or y1 + i > nr:
                    return False

                elif (x1 + i, y1 + i) == target:
                    return ['down', psource, int(math.hypot(x2-x1, y2-y1))]

                elif nmap[y1 + i][x1 + i] == 'O' or nmap[y1 + i][x1 + i] == 'S':
                    return False
        else:
            for i in xrange(0,laserrange + 1):
                if x1 - i < 0 or x1 - i > nc or y1 - i < 0 or y1 - i > nr:
                    return False

                elif (x1 - i, y1 - i) == target:
                    return ['up', psource, int(math.hypot(x2-x1, y2-y1))]

                elif nmap[y1 - i][x1 - i] == 'O' or nmap[y1 - i][x1 - i] == 'S':
                    return False

    return False

def getPath(relations, cordrelation, path):

    print cordrelation
    path.append(cordrelation[1])

    if cordrelation[0] == 0:
        return path

    for relation in relations:
        if cordrelation[2] == relation[1]:
            return getPath(relations, relation, path)

def getPotentialMoves(source,nmap):
    nr = len(nmap)
    nc = len(nmap[0])

    visited = Set()

    q = Queue.Queue()

    potential = []

    q.put([0,source,None])

    while not q.empty():
        current = q.get()
        count = current[0]
        x,y = current[1]

        cord = nmap[y][x]

        if current[1] not in visited:
            visited.add(current[1])

            if cord != 'V' and cord != 'O' and cord != 'S' and count == 2:
                potential.append(current)

            if cord != 'V' and cord != 'O' and cord != 'S' and count != 2:
                if x+1 < nc:
                    q.put([count+1,(x+1,y),current[1]])

                    if y+1 < nr:
                        q.put([count+1,(x+1,y+1),current[1]])

                if x-1 > 0:
                    q.put([count+1,(x-1,y),current[1]])

                    if y-1 > 0:
                        q.put([count+1,(x-1,y-1),current[1]])
                
                if y+1 < nr:
                    q.put([count+1,(x,y+1),current[1]])

                if y-1 > 0:
                    q.put([count+1,(x,y-1),current[1]])

                potential.append(current)

    return potential

def pathfinder(source,target,nmap,s):
    nr = len(nmap)
    nc = len(nmap[0])

    visited = Set()

    q = Queue.Queue()

    potential = []

    q.put([0,source,None])

    targetRelation = False

    while not q.empty():
        current = q.get()
        count = current[0]
        x,y = current[1]

        cord = nmap[y][x]

        if current[1] not in visited:
            visited.add(current[1])

            if current[1] == target:
                potential.append(current)
                targetRelation = current
                break;

            if cord != 'V' and cord != 'O' and (cord != 'S' or s):
                if x+1 < nc:
                    q.put([count+1,(x+1,y),current[1]])

                    if y+1 < nr:
                        q.put([count+1,(x+1,y+1),current[1]])

                if x-1 > 0:
                    q.put([count+1,(x-1,y),current[1]])

                    if y-1 > 0:
                        q.put([count+1,(x-1,y-1),current[1]])
                
                if y+1 < nr:
                    q.put([count+1,(x,y+1),current[1]])

                if y-1 > 0:
                    q.put([count+1,(x,y-1),current[1]])

                potential.append(current)

    potential.sort(key=lambda x: x[0], reverse=True)

    if targetRelation:
        path = getPath(potential,targetRelation,[])
        path.reverse()
        return path
    else:
        return False

def checkLaser(source,target,potential,nmap, level):
    cords = []
    hit = []
    direction = []
    for pot in potential:
        cords.append(pot[1])
        le = checkLaserHit(pot,target,4 + level,nmap)

        if le:
            #print pot
            #print le
            #print pot[1]
            hit.append(pot[1])
            direction.append(le)

    direction.sort(key=lambda x: x[2], reverse=True) 

    if direction:
        path = getPath(potential, direction[0][1], [])
        path.reverse()

        #print path
        return [path,direction[0][0]]
    else:
        return False

def checkDroid(source,target,potential,nmap):
    #cords = []
    # hit = []
    # direction = []
    # for pot in potential:
    #     cords.append(pot[1])
    #     le = checkDroidHit(pot,target,5,nmap)

    #     if le:
    #         #print pot
    #         #print le
    #         #print pot[1]
    #         hit.append(pot[1])
    #         direction.append(le)

    # direction.sort(key=lambda x: x[2], reverse=True) 

    # if direction:
    #     path = getPath(potential, direction[0][1], [])
    #     path.reverse()

    #     #print path
    #     return path
    # else:
    return False

def getClosestResource(source,nmap,rtype):
    x1, y1 = source

    nr = len(nmap)
    nc = len(nmap[0])

    res = []

    for y in xrange(nr):
        for x in xrange(nc):
            if nmap[y][x] == rtype:
                res.append((x,y))

    res2 = []
    for r in res:
        x2,y2 = r

        if r not in exhausted:
            res2.append([r,int(math.hypot(x2-x1,y2-y1))])

    res2.sort(key=lambda x: x[1])

    # for re in res2:
    #     print re

    if res2:
        return res2[0][0]
    else:
        return False

def exhaustedAdd(target):
    exhausted.add(target)

def translateSteps(path):
    steps = []

    if not path:
        return steps

    ns = 4
    if len(path) < 4:
        ns = len(path)



    currentPos = path[0]

    for i in xrange(1,ns):
        if currentPos[0] == path[i][0]:
            if currentPos[1] < path[i][1]:
                steps.append('left-down')
            else:
                steps.append('right-up')

        elif currentPos[1] == path[i][1]:
            if currentPos[0] < path[i][0]:
                steps.append('right-down')
            else:
                steps.append('left-up')

        elif currentPos[0] < path[i][0] and currentPos[1] < path[i][1]:
            steps.append('down')
        else:
            steps.append('up')

        currentPos = path[i]

    return steps

resources = [0,0]

sys.path.append("../api/python/")
import skyport

#assert(len(sys.argv) == 2)

# We open the socket manually
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('127.0.0.1', 54321))

inputbuf = "" # All received data goes in here
weapons_chosen = [] # remember the weapons we chose in the loadout

def read_packet(): # this AI takes a single-thread blocking-I/O approach
    global inputbuf
    try:
        ret = sock.recv(1)
        inputbuf += ret
        if not ret:
            print("Disconnected!")
            sys.exit(1)
    except socket.timeout as e:
        print("timeout!")
        return None
    except socket.error as e:
        print("error: %s" % e)
        sys.exit(1)
    try:
        characters_to_read = inputbuf.index("\n")
        line = inputbuf[0:characters_to_read] # removing the newline
        inputbuf = inputbuf[characters_to_read+1:len(inputbuf)]
        return line
    except ValueError as f:
        return None

def do_random_move():
    direction = random.choice(["up", "down", "left-down", "left-up", "right-down", "right-up"])
    print("moving %s-wards." % direction)
    transmitter.send_move(direction)

def shoot_mortar_in_random_direction():
    # Randomly performs invalid shots.
    # [-4, 4] x [-4, 4]
    j = random.randrange(-4, 5)
    k = random.randrange(-4, 5)
    if j == 0 and k == 0:
        j = 2 # don't hit ourselves -- we don't care about bias.
        k = 2
    transmitter.attack_mortar(j, k) # coordinates relative to us

def upgrade_random_weapon():
    #transmitter.upgrade("laser")
    transmitter.upgrade(random.choice(weapons_chosen))
    
def shoot_laser_in_random_direction():
    # requires you to select the laser as weapon, obviously
    direction = random.choice(["up", "down", "left-down", "left-up", "right-down", "right-up"])
    print("shooting %s-wards." % direction)
    transmitter.attack_laser(direction)

def shoot_droid_in_random_directions():
    directions = []
    for x in range(0, 8):
        directions.append(random.choice(["up", "down", "left-down", "left-up", "right-down", "right-up"]))
    print("shooting droid in sequence %r" % directions)
    transmitter.attack_droid(directions)
    
def send_line(line): # sends a line to the socket
    print("sending: '%s'" % line)
    if sock.sendall(line + "\n") != None:
        print("Error sending data!")

def got_handshake():
    print("got handshake!")

def got_error(errmsg):
    print("Error: '%s'" % errmsg)

def got_gamestate(turn, map_obj, player_list):
    if player_list[0]["name"] == 'BigSinep': # its our turn
        nmap = map_obj["data"]
        moves_left = 3

        #upgrade
        if resources[0] >= 4 and player_list[0]['primary-weapon']['level'] == 1:
            transmitter.upgrade('laser')
            resources[0] -= 4

        elif resources[0] >= 5 and player_list[0]['primary-weapon']['level'] == 2:
            transmitter.upgrade('laser')
            resources[0] -= 5

        if resources[1] >= 4 and player_list[0]['secondary-weapon']['level'] == 1:
            transmitter.upgrade('droid')
            resources[1] -= 4

        elif resources[1] >= 5 and player_list[0]['secondary-weapon']['level'] == 2:
            transmitter.upgrade('droid')
            resources[1] -= 5

        #get playerpos
        playerPos = player_list[0]["position"].split(', ')
        playerPos = (int(playerPos[1]), int(playerPos[0]))

        potential = getPotentialMoves(playerPos,nmap)
        #print checkDroid(src,target,potential,nmap)

        targets = []
        for Player in player_list:
            if not Player["name"] == 'BigSinep':
                targets.append(Player)

        targetfound = False

        targetCords = []

        x1,y1 = playerPos

        for target in targets:
            targetPos = target["position"].split(', ')
            targetPos = (int(targetPos[1]), int(targetPos[0]))

            #x2,y2 = targetPos

            #targetCords.append([targetPos,int(math.hypot(x2-x1, y2-y1))])
            targetCords.append([targetPos,target['health']])

        targetCords.sort(key=lambda x: x[1])

        for target in targetCords:
            targetPos = target[0]

            x,y = targetPos
            
            #check if target is in spawn area
            if nmap[y][x] != 'S':
                laser = checkLaser(playerPos,targetPos,potential,nmap,player_list[0]['primary-weapon']['level'])

                if laser:
                    targetfound = True
                    break

        #Shoot target
        if targetfound:
            for direction in translateSteps(laser[0]):
                transmitter.send_move(direction)

            #print("shooting %s-wards." % laser[1])
            transmitter.attack_laser(laser[1])

        #Move to closest resource and mine
        else:
            mine = False
            primary = True

            #get closest resource position
            if player_list[0]['primary-weapon']['level'] == 3 and player_list[0]['secondary-weapon']['level'] == 3:
                #all upgraded, time to hunt
                targetPos = targetCords[0]

            # elif player_list[0]['primary-weapon']['level'] == 3:
            #     #laser done, get droid material
            #     targetPos = getClosestResource(playerPos,nmap,'C')
            #     mine = True
            #     primary = False

            else:
                #get laser material
                targetPos = getClosestResource(playerPos,nmap,'R')
                mine = True
            
            if targetPos:
                #todo check if walking into range of other player

                #Generate path to resource
                # a = pathfinding.AStar(nmap, playerPos, targetPos)
                # a.generatePath()
                x,y = playerPos

                if nmap[y][x] == 'S':
                    path = pathfinder(playerPos,targetPos,nmap,True)
                else:
                    path = pathfinder(playerPos,targetPos,nmap,False)

                directions = []

                if path:
                    directions = translateSteps(path)
                    for direction in directions:
                        transmitter.send_move(direction)

                print mine
                print targetPos

                if mine:
                    if len(directions) < 3 and len(directions) > 0:
                        for i in xrange(3 - len(directions)):
                            transmitter.mine()
                            #add to exhausted list
                            if primary:
                                resources[0] += 1
                            else:
                                resources[1] += 1

                    elif len(directions) == 0:
                        for i in xrange(0,2):
                            transmitter.mine()
                            if primary:
                                resources[0] += 1
                            else:
                                resources[1] += 1
                
            #todo: do potential moves
            # randls = getPotentialMoves(playerPos,nmap)
            # dirs = translateSteps(randls)
            # transmitter.send_move(dirs[1])
            do_random_move()



                #transmitter.upgrade(
                # do_random_move()
                # do_random_move()
                # #transmitter.mine()
                # random.choice([transmitter.mine, shoot_mortar_in_random_direction,
                #                shoot_laser_in_random_direction, shoot_droid_in_random_directions,
                #                transmitter.mine, upgrade_random_weapon])()
        

def got_gamestart(turn, map_obj, player_list):
    # print(map_obj)
    # weapons = ["mortar", "droid", "laser"]
    # primary_weapon = random.choice(weapons)
    # weapons.remove(primary_weapon)
    # secondary_weapon = random.choice(weapons)
    # print("chose loadout: %s and %s" % (primary_weapon, secondary_weapon))
    # global weapons_chosen
    # weapons_chosen = [primary_weapon, secondary_weapon]
    #transmitter.send_loadout(primary_weapon, secondary_weapon)
    transmitter.send_loadout("laser", "droid")

def got_action(action_type, who, rest_data):
    print("got action!")

def got_endturn():
    print("got endturn!")

    
receiver = skyport.SkyportReceiver()
transmitter = skyport.SkyportTransmitter(send_line)
# the SkyportTransmitter doesn't do networking on its own
# so you have to provide it with a send_line function that
# it can use to send data to the socket


# Register functions as callback, so that
# SkyportReceiver can call them when something happens
receiver.handler_handshake_successful = got_handshake
receiver.handler_error = got_error
receiver.handler_gamestate = got_gamestate
receiver.handler_gamestart = got_gamestart
receiver.handler_action = got_action
receiver.handler_endturn = got_endturn

# send the initial handshake
transmitter.send_handshake("BigSinep")

while True:
    line = read_packet() # try to read a line from the socket
    if line != None:
        print("got line: '%r'" % line)
        receiver.parse_line(line) # hand the line to SkyportReceiver to process

